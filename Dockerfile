# Sử dụng một image cơ sở Java 17
FROM openjdk:17

# Thiết lập thư mục làm việc
WORKDIR /app


COPY .mvn/ .mvn
COPY mvnw pom.xml ./
RUN chmod 777 -R ./mvnw
RUN ./mvnw dependency:go-offline
COPY ./src ./src
RUN ./mvnw clean install -DskipTests

# Mở cổng cho ứng dụng Spring Boot
EXPOSE 8080

# Chạy ứng dụng Spring Boot khi container được khởi chạy
CMD ["java", "-jar", "target/demo2-0.0.1-SNAPSHOT.jar"]




#FROM eclipse-temurin:17-jdk-alpine as builder
#WORKDIR /app
#COPY .mvn/ .mvn
#COPY mvnw pom.xml ./
#RUN chmod 777 -R ./mvnw
#RUN ./mvnw dependency:go-offline
#COPY ./src ./src
#RUN ./mvnw clean install -DskipTests
#
#FROM eclipse-temurin:17-jre-alpine
#WORKDIR /app
#EXPOSE 8080
#COPY --from=builder /app/target/*.jar demo.jar
#ENTRYPOINT ["sh", "-c", "java -jar demo.jar"]



